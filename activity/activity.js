let fullNameDisplay = document.querySelector("#full-name-display")
let inputFirstName = document.querySelector("#txt-first-name")
let inputLastName = document.querySelector("#txt-last-name")
let submitButton = document.querySelector("#submit-btn")

const showName = () => {
	/*console.log(inputFirstName.value)
	console.log(inputLastName.value)*/
	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
}

inputFirstName.addEventListener('keyup',showName)
inputLastName.addEventListener('keyup',showName)


submitButton.addEventListener('click',()=>{
	if(inputFirstName.value !== "" && inputLastName.value !== ""){
		alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}.`)
		inputFirstName.value = ""
		inputLastName.value = ""
		fullNameDisplay.innerHTML = ""
		window.location.href = "http://zuitt.co"
	} else {
		alert(`Please input firstname and lastname.`)
	}
})